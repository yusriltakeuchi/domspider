import requests
import yaml
import time
import sys
import os
from urllib.parse import urlparse
from bs4 import BeautifulSoup

'''
    DomSpider - Domspider is a domain crawling tools 
    which one can crawling every domain name in the internet
    @author: Yusril Rapsanjani
    @site: www.yurain.me
'''

class Spider:
    def __init__(self, max_domain):
        self.domain_list = []
        self.max = max_domain
        self.stop = False
        self.star_time = time.time()

        #Ini cuman kode warna aja
        self.CRED = '\033[91m'
        self.CWHITE  = '\33[37m'
        self.CGREEN  = '\33[32m'
        self.CYELLOW = '\33[33m'

    #Bot akan menjelajahi setiap url
    #yang ia dapatkan untuk mendapatkan url baru
    #untuk menambahkan nama domainnya ke list.
    def travel(self, url):
        try:
            #Memulai request ke url
            r = requests.get(url)
            #BeautifulSoup memparsing HTML elementnya
            soup = BeautifulSoup(r.content, 'html.parser')
            #Mencari segala element <a href
            link = soup.findAll('a', href=True)

            #just temporary list
            temp_domain = []
            #Looping semua element yang didapat
            for trash in link:
                try:
                    #Spesifik mendapatkan element href
                    rawUrl = trash['href']
                    #Trash url tersebut akan diparsing
                    #menjadi lebih rapih
                    url = urlparse(rawUrl.strip())
                    #Sehingga hasilnya www.domain.com
                    domain = '{uri.netloc}'.format(uri=url)
                    if domain:
                        #Jika domain yang didapat belum
                        #pernah ada di temporary domain list
                        if domain not in temp_domain:
                            #Jika temporary kurang dari maximum
                            if len(temp_domain) < self.max:
                                if len(self.domain_list) > 0:
                                    #Memastikan pula apakah domain
                                    #sudah pernah ada di domain list
                                    #atau belum.
                                    if domain not in self.domain_list:
                                        temp_domain.append(domain)
                                else:
                                    temp_domain.append(domain)
                            else:
                                break
                except Exception:
                    continue

            #Mengeluarkan hasil domain 
            #yang terkumpul dari temporary domain
            #setelah melalui berbagai proses
            #pemfilteran
            for temp in temp_domain:
                if temp not in self.domain_list:
                    if len(self.domain_list) < self.max:
                        self.domain_list.append(temp)
                        print("{}[{}] {}{} {}[Domain gained]"
                            .format(self.CYELLOW, len(self.domain_list), self.CRED, temp, self.CGREEN))
                    
            #Jika domain list mencapai max
            #maka proses crawling akan berhenti
            if len(self.domain_list) == self.max:
                if self.stop == False:
                    self.finalResult()
                    self.stop = True

            if self.stop == False:
                #Travel another link again
                for temp in temp_domain:
                    next_url = ("http://{}".format(temp))
                    self.travel(next_url)
        except Exception: 
            null = 0
        
    
    #Ketika spider telah menyelesaikan
    #tugasnyas dalam konteks ia telah
    #berhasil mengumpulkan list domains
    #berdasarkan max_domain
    def finalResult(self):
        end_time = time.time()
        calc_time = round(end_time - self.star_time, 2)
        
        #Penentuan waktu
        interval = ""
        if calc_time < 60:
            interval = "seconds"
        elif calc_time > 60 and calc_time < 3600:
            calc_time = round(calc_time / 60, 2)
            interval = "minutes"
        else:
            calc_time = round(calc_time / 3600, 2)
            interval = "hours"
        
        result_time = ("{} {}".format(str(calc_time), interval))
        print("-----------------------------------------")
        print("{}{} {}domain gained with times {}{}{}"
            .format(self.CRED, len(self.domain_list), self.CGREEN, self.CYELLOW, result_time, self.CGREEN))
        print("-----------------------------------------")
        self.ExportYaml()
        sys.exit()

    #Export hasil domain yang terkumpul
    #ke dalam sebuah file bernama domains.yml
    def ExportYaml(self):
        files = 'domains.yml'
        if os.path.isfile(files):
            os.remove(files)
        with open(files, 'w') as yaml_file:
            yaml.dump(self.domain_list, stream=yaml_file, default_flow_style=False)