from spider import Spider

'''
    DomSpider - Domspider is a domain crawling tools 
    which one can crawling every domain name in the internet
    @author: Yusril Rapsanjani
    @site: www.yurain.me
'''

#Starting url
start_url = 'https://moz.com/top500'
#Maximum domain yang ingin diambil
max_domain = 100

#Membuat object spider
spider = Spider(max_domain)
#Memulai travel spider untuk menjelajah internet
spider.travel(start_url)