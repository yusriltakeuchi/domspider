# Domspider

Domspider is a domain crawling tools which one can crawling every domain name on the internet.
It's fast and powerful to collect every domain on the internet with just a few seconds.

# Requirements
- pip install requests
- pip install PyYAML
- pip install urllib3
- pip install beautifulsoup4

# Creator
- Author: Yusril Rapsanjani
- Language use: Python3
- Facebook: http://facebook.com/yuranitakeuchi


# Screenshots
- ![](spider_tools.png)
- ![](spider_yaml.png)